package pl7;

import java.util.Scanner;

public class ConversorVarios {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        String rep;
        int base;

        System.out.println("Introduza um número em base décimal: ");
        int num = input.nextInt();

        do {
            System.out.println("Pretende converte-lo para qual base?\n(Escolha um valor entre 1 e 16)");
            base = input.nextInt();
        } while ((base < 1) && (base > 16));

        do {
            if (base < 10) {
                conversorBasico(num, base);
                System.out.println("O valor " + num + " representa " + conversorBasico(num, base) + " em base " + base);
            }
            System.out.println("Deseja repetir a iteração? (Sim/Não)");
            rep = input.next();
            input.nextLine();

        } while (rep.equalsIgnoreCase("sim"));
    }

    public static String conversorBasico(int num, int base) {
        int dig, ordem = 0;
        String novo = "";

        do {
            dig = (num % base);
            num = (num / base);
            novo = dig + novo;
            ordem++;

        } while (num > 0);

        return novo;
    }
}
