package pl7;

import java.util.Scanner;

public class Ex1 {

    public static void main(String[] args) {

        int i, s = 0, c = 0, menor, menorIndice = 0;
        int[] v = new int[10];

        Scanner ler = new Scanner(System.in);

        for (i = 0; i < v.length; i++) {
            System.out.print("Introduza o " + (i + 1) + "º número: ");
            v[i] = ler.nextInt();
        }

        for (i = 0; i < v.length; i++) {
            if (v[i] % 2 == 0) {
                s = s + v[i];
                c++;
            }
        }

        menor(v);
        indice(v, menor(v));
        
        if (c != 0) {
            System.out.println("A média dos numeros pares na sequência inttroduzida é " + (double) s / c);
        } else {
            System.out.println("Operação impossível de realizar");
        }
    }

    public static int menor(int[] v) {
        int menor = v[0], menorIndice = 0;

        for (int i = 0; i < v.length; i++) {
            if (v[i] < menor) {
                menor = v[i];
                menorIndice = i;
            }
        }
        return menor;
    }

    public static void indice(int[] v, int menor) {

        for (int i = 0; i < v.length; i++) {
            if (v[i] == menor) {
                System.out.println("Indice do menor: "+(i+1));
            }
        }
    }
}
