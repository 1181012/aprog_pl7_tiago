package pl7;

import java.util.Scanner;

public class Ex4 {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int nElem = 0;

        do {
            System.out.println("Quantos números pretende ler? ");
            nElem = input.nextInt();
        } while (nElem <= 0);
        int[] valor = new int[nElem];

        for (int i = 0; i < nElem; i++) {
            System.out.println("Introduza o " + (i + 1) + "º número: ");
            valor[i] = input.nextInt();
        }
        inversao(valor);
        System.out.println("Output: ");
        resultados(valor, nElem);
        System.out.println(" ");
        direita(valor, nElem);
        resultados(valor, nElem);
    }

    public static void inversao(int[] valor) {

        int p = (valor.length) / 2;
        int fim = valor.length - 1;

        for (int i = 0; i < (p); i++) {
            int aux = 0;

            aux = valor[i];
            valor[i] = valor[fim - i];
            valor[fim - i] = aux;
        }

    }

    public static void resultados(int[] valor, int nElem) {
        for (int i = 0; i < nElem; i++) {
            System.out.print(valor[i] + " ");
        }
    }

    public static void direita(int[] valor, int nElem) {
        int aux = 0;
        aux = valor[nElem - 1];

        for (int i = nElem - 1; i > 0; i--) {
            valor[i] = valor[i - 1];
        }
        valor[0] = aux;
    }
}
