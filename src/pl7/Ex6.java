package pl7;

import java.util.Scanner;

public class Ex6 {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        String vec[] = new String[100];
        int nElem = 0;
        boolean flag;
        int opçao;
        

        String texto = "1 - Inserir um visitante; \n2 - Listar todos os visitantes; \n3 - Atualizar um nome dado; \n4 - Eliminar um visitante dado; \n5 - Listar os nomes começados por uma dada letra; \n6 - Listar os nomes repetidos; \n7 - Sair \n \nDigite a sua opçao: ";

        do {
            System.out.println(texto);
            opçao = input.nextInt();
            input.nextLine();
            switch (opçao) {
                case 1:
                    nElem = inserirVisitante(vec, nElem);
                    break;
                case 2:
                    listarVisitantes(vec, nElem);
                    break;
                case 3:
                    flag = atualizarNome(vec, nElem);
                    if (flag == true) {
                        System.out.println("Nome atualizado com sucesso.");
                    }
                    break;
                case 4:
                    flag = eliminarVisitante(vec, nElem);
                    if (flag == true) {
                        nElem--;
                        System.out.println("Nome removido com sucesso.");
                    }
                    break;
                case 5:
                    if (nElem>0){
                    listarComeçados(vec, nElem);
                    }else System.out.println("Não existem nomes inseridos. Para inserir um novo nome escolha a opção 1.");
                    break;
                case 6:
                    listarRepetidos(vec, nElem);       
                    break;
                default:
                    System.out.println("Valor Inválido.");
            }
        } while (opçao != 7);
    }

    public static int inserirVisitante(String[] vec, int nElem) {
        int i = nElem;

        System.out.println("Introduza o nome do visitante: ");
        vec[i] = input.nextLine();
        nElem++;

        return nElem;

    }

    public static void listarVisitantes(String[] vec, int nElem) {
        String lista = "";

        if (nElem > 0) {

            for (int i = 0; i < nElem; i++) {
                lista = lista + " ||  " + vec[i];
            }
            System.out.println(lista);
        } else {
            System.out.println("Não existem visitantes para serem listados.");
        }
    }

    public static boolean atualizarNome(String[] vec, int nElem) {

        int i = 0;
        boolean flag = false;

        if (nElem > 0) {

            System.out.println("Qual é o nome atual que pretende atualizar? ");
            String nome = input.nextLine();

            System.out.println("Pretende atualizar " + nome + " para qual nome? ");
            String novo = input.nextLine();

            if (procurar(vec, nElem, nome) == -1) {
                System.out.println("Não existe nenhum utilizador com o Nome: " + nome + ". Para introduzir um novo visitante seleione a opção 1.");
            } else {
                flag = true;
                vec[procurar(vec, nElem, nome)] = novo;
            }

        } else {
            System.out.println("Não existem visitantes para serem atualizados. Para introduzir um novo visitante selecione a opção 1.");

        }
        return flag;
    }

    public static boolean eliminarVisitante(String[] vec, int nElem) {

        boolean flag = false;

        if (nElem > 0) {
            System.out.println("Qual é o nome do visitante que pretende excluir? ");
            String nome = input.nextLine();

            int indice = procurar(vec, nElem, nome);

            if (indice == -1) {
                System.out.println("Não existem visitantes com esse nome.");
            } else {
                for (int i = indice; i < nElem - 1; i++) {
                    vec[i] = vec[i + 1];
                }
                flag = true;
            }
        } else {
            System.out.println("Não existem visitantes para excluir. Para introduzir um novo visitante selecione a opção 1.");
        }

        return flag;
    }

    public static void listarComeçados(String[] vec, int nElem) {

        System.out.println("Deseja analisar os visitantes cujo nome começa por qual letra? ");
        String palavra = input.next();
        char letra = palavra.charAt(0);
        String mostrar = "";
        boolean flag=false;

        for (int i = 0; i < nElem; i++) {
            if (Character.toUpperCase(vec[i].charAt(0)) == (Character.toUpperCase((letra)))) {
                mostrar = mostrar + " | " + vec[i];
                flag=true;
            }
        }
        if (flag==true){
        System.out.println("Lista de visitantes cujo nome começa pela letra " + letra);
        System.out.println(mostrar);
        }else System.out.println("Não existem visitantes cujo nome começa pela letra : "+(Character.toUpperCase((letra))));
    }

    public static void listarRepetidos(String[] vec, int nElem) {
        boolean flag = false;
        int repetidos = 0;
        String[] nomesRepetidos = new String[nElem / 2];
        for (int primeiro = 0; primeiro < nElem-1; primeiro++) {
            for (int segundo = (primeiro + 1); segundo < nElem; segundo++) {

                if (vec[primeiro].equals(vec[segundo])) {
                    if ((procurar(nomesRepetidos, repetidos, vec[segundo])) == -1) {
                        nomesRepetidos[repetidos] = vec[segundo];
                        repetidos++;
                    }
                }
            }
        }
        if(repetidos>0){
        resultados(nomesRepetidos, repetidos);
        }else System.out.println("Não existem nomes repetidos.");
    }

    public static int procurar(String[] vec, int nElem, String nome) {

        for (int i = 0; i < nElem; i++) {
            if (vec[i].equalsIgnoreCase(nome)) {
                return i;
            }
        }
        return -1;
    }
    
    public static void resultados(String[]vec, int nElem){
        for (int i = 0; i < nElem; i++) {
            System.out.print(vec[i]+", ");
        }
        System.out.println();
    }
}
