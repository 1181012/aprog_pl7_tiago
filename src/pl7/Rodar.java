package pl7;

import java.util.Scanner;

public class Rodar {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        String direcao, tmp;
        int vezes, vezesFim;

        int qnt = lerIntPos("Quantos números pretende introduzir? ");

        int[] vec = new int[qnt];
        lerNum(vec, qnt);

        do {
            System.out.println("Em qual direção deseja rotacionar os números introduzidos? (Esquerda ou Direita)");
            tmp = input.next();
        } while ((!tmp.equalsIgnoreCase("esquerda")) && (!tmp.equalsIgnoreCase("direita")));

        direcao = tmp.toLowerCase();

        vezes = lerIntPos("Quantas vezes pretende rotacionar para a " + direcao + "?");
        
        System.out.println("Valores Iniciais: ");
        resultados(vec, qnt);
        System.out.println();

        vezesFim = preRotacao(direcao, vec, vezes);

        if (vezes != vezesFim) {
            if (direcao.equals("direita")) {
                direcao = "esquerda";
            } else {
                direcao = "direita";
            }
        }

        rotacao(direcao, vezes, vec);

        System.out.println();

        System.out.println("Valores após serem rotacionados: ");
        resultados(vec, qnt);

    }

    public static void lerNum(int[] vec, int qnt) {

        for (int i = 0; i < qnt; i++) {

            System.out.println("Introduza o " + (i + 1) + "º número.");
            vec[i] = input.nextInt();

            input.nextLine();
        }
    }

    public static int preRotacao(String direcao, int[] vec, int vezes) {
        int metade = vec.length / 2;

        if (vezes > metade) {
            vezes = vec.length - vezes;
        }
        return vezes;

    }

    public static void rotacao(String direcao, int vezes, int[] vec) {
        int numeros = vec.length, aux;
        if (direcao.equals("direita")) {

            for (int j = 0; j < vezes; j++) {
                aux = vec[0];

                for (int i = 0; i < vec.length - 1; i++) {
                    vec[i] = vec[i + 1];
                }
                vec[vec.length - 1] = aux;
            }
        } else {
            for (int j = 0; j < vezes; j++) {
                aux = vec[vec.length - 1];

                for (int i = vec.length - 1; i > 0; i--) {
                    vec[i] = vec[i - 1];
                }
                vec[0] = aux;
            }
        }
    }

    public static void resultados(int[] vec, int qnt) {

        for (int i = 0; i < qnt; i++) {
            System.out.print(vec[i] + " ");

        }
    }

    public static int lerIntPos(String msg) {
        int num;

        do {
            System.out.println(msg);
            num = input.nextInt();
        } while (num <= 0);

        return num;
    }

}
