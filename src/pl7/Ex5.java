package pl7;

import java.util.Scanner;

public class Ex5 {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        int cont = 0;
        String[] nomes = new String[100];
        int nElem = lerNomes(nomes);

        System.out.println("Lista de nomes: ");
        mostrarListagem(nomes, nElem);

        for (int i = 0; i < nElem; i++) {

            String apelido = apelido(nomes[i]);

            if ((apelido.charAt(0) == 'S') || apelido.charAt(0) == 's') {
                System.out.println("O apelido de  " + nomes[i] + " é : " + apelido);
                cont++;
            }
        }
        double perc = cont / nElem;

        if (nElem > 0) {
            System.out.println("A percentagem de apelidos começados por S é de " + perc + "%.");
        }
    }

    public static int lerNomes(String[] nomes) {
        int i = 0;

        System.out.println("Introduza o 1º nome:   \nPara terminar digite 'FIM'");
        nomes[i] = input.nextLine();

        while (!nomes[i].equalsIgnoreCase("Fim") && i < nomes.length) {

            i++;

            System.out.println("Introduza o " + (i + 1) + "º nome:   \nPara terminar digite 'FIM'");
            nomes[i] = input.nextLine();

        }
        return i;
    }

    public static void mostrarListagem(String[] nomes, int nElem) {

        for (int i = 0; i < nElem; i++) {
            System.out.println((i + 1) + "º Nome: " + nomes[i]);
        }
    }

    public static String apelido(String nomeC) {

        String apelido = "";
        int espaco = 0, i = nomeC.length() - 1;
        boolean flag = false;

        do {
            if (nomeC.charAt(i) == ' ') {
                espaco = i;
                flag = true;
            } else {
                i--;
            }
        } while ((flag == false) && (i > 0));

        if (flag = true) {
            for (int p = espaco + 1; p < nomeC.length(); p++) {
                apelido = apelido + nomeC.charAt(p);
            }
            return apelido;
        } else {
            return "";
        }
    }

}
