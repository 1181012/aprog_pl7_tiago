package pl7;

import java.util.Scanner;

public class Ex3 {

    static Scanner texto = new Scanner(System.in);
    static Scanner valor = new Scanner(System.in);

    public static void main(String[] args) {

        int nElem = 0;
        String nomes[] = new String[100];
        String m = ("1-Ler Nomes\n2-Enigma Nome\n3-Terminar\n\nEscolha uma opção: ");

        char op;

        do {
            System.out.println(m);
            op = valor.next().charAt(0);
            switch (op) {
                case '1':
                    nElem = lerNomes(nomes, nElem);
                    break;
                    
                case '2':
                    if (nElem > 0) {
                        System.out.println("Nome:");
                        String nome = texto.nextLine();
                        int resultado = enigma(nomes, nome, nElem);
                        if (resultado == 0) {
                            System.out.println("Não há nenhum nome igual ao nome introduzido.");
                        } else {
                            System.out.println("O nome " + nome + " encontra-se na " + (resultado + 1) + "º posição.");
                        }
                    } else {
                        System.out.println("Não há nomes para que possa ser feita a comparação.");
                    }
                    break;
                    
                case '3':
                    break;
                default:
                    System.out.println("Opção inválida!!");
            }
        } while (op != '3');
    }

    private static int lerNomes(String[] vec, int nElem) {

        System.out.println("Introduza um nome:\nPara terminar digite 'Fim'");
        vec[nElem] = valor.next();

        do {
            nElem++;

            System.out.println("Introduza um nome: \nPara terminar digite 'Fim'");
            vec[nElem] = valor.next();

        } while (!vec[nElem].equalsIgnoreCase("fim") && (nElem < vec.length));

        return nElem;
    }

//DUVIDA - PARA QUE SERVE O LISTAR?
    private static String listar(String[] vec, int n) {
        String lista = "";

        for (int i = 0; i < n; i++) {
            lista = lista + vec[i] + " ";
        }
        return lista;
    }

    private static int enigma(String[] nomes, String nome, int nElem) {
        int resultado = 0;

        for (int i = 0; i < nElem; i++) {
            
            if (nomes[i].equalsIgnoreCase(nome)) {
                resultado = i;
            }
        }
        return resultado;
    }

}
