package pl7;

import java.util.Scanner;

public class CompEx1 {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        int[] vec = new int[100];

        System.out.println("N: ");
        int num = input.nextInt();

        ler(vec, num);
        int media = media(vec,num);
        verificar(vec,num,media);

    }

    public static void ler(int[] vec, int num) {

        for (int i = 0; i < num; i++) {
            System.out.println((i + 1) + "º Nº: ");
            vec[i] = input.nextInt();
        }
    }

    public static int media(int[] vec, int num) {
        int soma = 0;

        for (int i = 0; i < num; i++) {
            soma = soma + vec[i];
        }
        return (soma / num);
    }

    public static void verificar(int[] vec, int num, int media) {
        System.out.println("Valores inferiores à média");
        System.out.println("Posição | Valor");
        
        for (int i = 0; i < num; i++) {
            if (vec[i] > media){
                System.out.println(+i+"º - "+(vec[i]));
            }
        }
    }
}
