package pl7;

import java.util.Scanner;

public class Ex2 {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        final int MAX = 20;
        double[] venc = new double[MAX];
        String[] nome = new String[MAX];

        int nElem = ler(nome, venc);

        if (nElem > 0) {

            double valorMedia = media(nElem, venc);

            double num = lerDoublePos("Vencimento limite: ");

            double menores = menorPercentagem(num, nElem, venc);
            double perc = (menores / nElem);

            System.out.println(listagem(nElem, valorMedia, nome, venc));
            System.out.format("%.2f%%", (perc * 100));

        } else {
            System.out.println("sem dados");
        }
    }

    public static int ler(String[] nome, double[] venc) {
        int i = 0;

        System.out.print("Nome: ");
        nome[i] = input.nextLine();

        while (i < nome.length && !nome[i].equals("tt")) {

            venc[i] = lerDoublePos("Vencimento: ");
            i++;
            if (i < nome.length) {

                System.out.print("Nome: ");
                nome[i] = input.nextLine();
            }
        }

        return i;
    }

    public static double lerDoublePos(String text) {
        double num;
        do {
            System.out.print(text);
            num = input.nextDouble();
            input.nextLine();
        } while (num < 0);

        return num;
    }

    public static double media(int nElem, double[] venc) {
        double soma = 0;
        for (int i = 0; i < venc.length; i++) {
            soma = soma + venc[i];
        }
        return (soma / nElem);
    }

    public static String listagem(int num, double media, String[] nome, double[] venc) {
        String pobres = "";

        for (int i = 0; i < num; i++) {
            if (venc[i] < media) {
                pobres = pobres + nome[i] + " ";
            }
        }
        return pobres;
    }

    public static int menorPercentagem(double valor, int num, double[] venc) {
        int cont = 0;

        for (int i = 0; i < num; i++) {
            if (venc[i] < valor) {
                cont++;
            }
        }
        return (cont);
    }
}
